@extends('layouts.mrx')

@section('content')
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque, elit at faucibus venenatis, ligula diam varius purus, sed ornare turpis enim ut urna. Nullam commodo lectus non ex pretium, ac ullamcorper ante consectetur. Maecenas interdum tortor lobortis magna vulputate laoreet. Vivamus quis imperdiet nisi. Aenean at tempor turpis. Aliquam ut fermentum ex. Vivamus auctor enim elit, eget mollis nulla vestibulum tincidunt. Etiam dictum risus ac arcu laoreet, a tempor urna dapibus.
    </p>
    <p>
        Praesent aliquet a magna sed lacinia. Etiam fringilla ultricies enim sit amet porttitor. Mauris mauris libero, porttitor eget gravida a, molestie et neque. Donec pellentesque interdum diam non hendrerit. Nunc condimentum, justo sit amet tempus pellentesque, justo eros interdum enim, in tempus ipsum dolor vel odio. Vivamus hendrerit, tortor pharetra accumsan rhoncus, neque magna ornare ex, quis mattis leo nisi vel erat. Cras mi purus, hendrerit eget condimentum vel, malesuada non lorem. Sed at enim felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur egestas rutrum dapibus. Donec sed tellus id augue ornare tristique. Etiam sed odio vehicula, ornare sapien quis, tincidunt lectus. Nam ullamcorper ligula eget libero commodo fermentum. Sed condimentum nulla sed arcu ullamcorper eleifend. Cras ornare ex quis eros fringilla blandit. Nunc sem lorem, sodales quis rutrum nec, ornare in tellus.
    </p>
    <p>
        Donec sit amet odio efficitur, pharetra neque et, ultrices enim. Morbi a erat luctus, rutrum eros a, sodales mauris. Sed ultrices molestie neque, a faucibus tellus convallis nec. Aliquam gravida leo sed odio auctor, id euismod justo tristique. Sed quis ultricies velit. Phasellus a fringilla nisl, sit amet mattis arcu. Nulla facilisi. In accumsan consequat erat, quis volutpat ante faucibus ut. Nam eget urna quis justo commodo consectetur. Sed nec tempor eros. Duis maximus pharetra ultrices. Vivamus nunc ante, sollicitudin nec fringilla et, pellentesque ut leo.
    </p>
    <p>
        Mauris sapien lacus, maximus non eros ac, pulvinar scelerisque elit. Vivamus interdum elit a neque lacinia pharetra. Aliquam id neque non ipsum blandit fringilla. Duis arcu turpis, scelerisque quis ultrices euismod, lobortis vel ex. Donec finibus elit odio, id fermentum ex finibus a. Maecenas maximus nisl vitae purus molestie, non aliquet neque ultrices. Integer arcu libero, rhoncus non ante a, ullamcorper gravida purus. Nam varius lorem at magna sodales, congue placerat nisi rutrum. Nunc luctus sodales laoreet. Integer ultrices ante ut ligula feugiat vestibulum. Cras non neque ut tortor volutpat tempor sit amet et nunc.
    </p>
    <p>
        Nullam non maximus risus. Nunc efficitur tortor quis justo semper malesuada. Aliquam erat ante, sollicitudin ac dui in, sollicitudin condimentum sem. Duis tempus cursus odio ac auctor. Aliquam viverra massa nec ligula semper vulputate. Donec suscipit faucibus leo sed pretium. Aenean vehicula, purus sed luctus congue, nulla purus scelerisque felis, sit amet porta eros ipsum at neque. Vivamus nunc est, commodo vitae ante et, tincidunt iaculis ipsum. Aliquam vehicula mauris vel pharetra dictum.
    </p>
@endsection
