<link rel="stylesheet" href="src/dist/fonts/font-awesome.min.css">
<link rel="stylesheet" href="src/dist/fonts/login.min.css">
<div class="main">
    <div class="container">
        <center>
            <div class="middle">
                <div id="login">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <fieldset class="clearfix">
                            <p ><span class="fa fa-user"></span><input type="text"  Placeholder="Username" name="username" required></p> <!-- JS because of IE support; better: placeholder="Username" -->
                            <p><span class="fa fa-lock"></span><input type="password"  Placeholder="Password" name="password" required></p> <!-- JS because of IE support; better: placeholder="Password" -->
                            <div>
                                <span style="width:48%; text-align:left;  display: inline-block;"><a class="small-text" href="#">Forgot
                                    password?</a></span>
                                    <span style="width:50%; text-align:right;  display: inline-block;"><input type="submit" value="Sign In"></span>
                                </div>
                            </fieldset>
                            <div class="clearfix"></div>
                        </form>
                        <div class="clearfix"></div>
                    </div> <!-- end login -->
                    <div class="logo">LAB. RPL <br>
                        <p class="small pull-left">Jurusan Matematika</p>
                        <div class="clearfix"></div>
                    </div>



                </div>
            </center>
        </div>

    </div>
