@extends('layouts.mrx')
@section('content')
    <div class="box">
        <div class="box-body">
            <label>Prodi :</label>
            <select class="form-control" disabled>
                <option>Matematika</option>
                <option>Statistika</option>
                <option>Ilmu Komputer</option>
            </select>
            <br>
            <div class="modal fade" id="modal-default">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Default Modal</h4>
                  </div>
                  <div class="modal-body">
                    <form class="form-horizontal" action="index.html" method="post">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Kode</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Masukkan Kode Program Studi">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control"  placeholder="Masukkan Nama Program Studi">
                          </div>
                        </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Nim</th>
                <th>Nama</th>
                <th>Respon</th>
                <th>TP</th>
                <th>Tugas Praktikum</th>
                <th>Nilai Akhir</th>
              </tr>
              </thead>
              <tbody>
                  @foreach ($datas->students as $data)
                      <tr>
                          <td>{{$data->code}}</td>
                          <td>{{$data->name}}</td>
                          <td>{{$data->responsValue}}</td>
                          <td>{{$data->preliminaryAssignmentValue}}</td>
                          <td>{{$data->practiceValue}}</td>
                          <td>{{$data->total}}</td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
        </div>
    </div>
@endsection
