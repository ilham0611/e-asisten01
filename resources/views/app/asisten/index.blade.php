@extends('layouts.mrx')

@section('content')
    <div class="row">
        @foreach ($users as $user)
            <div class="col-md-4">
              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-mrx-red">
                  <h3 class="widget-user-username text-black">{{ $user->name }}</h3>
                  <h5 class="widget-user-desc text-black">{{ $user->username }}</h5>
                </div>
                <div class="widget-user-image">
                  <img class="img-circle" src="{{ asset('img/anonymous.jpg')}}" alt="User Avatar">
                </div>
                <div class="box-footer">

                  <!-- /.row -->
                </div>
              </div>
              <!-- /.widget-user -->
            </div>
        @endforeach
      </div>
@endsection
