<?php

namespace App\Model\App\Master;

use App\Model\App\Master\Assistant;
use App\Model\App\Master\Value;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;
    public $incrementing  = false;
    protected $dates = ['deleted_at'];
    protected $table = 'master.students';
    protected $connection = 'Master';
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function assistant() {
        return $this->belongsTo(Assistant::class, 'assistant_id');
    }
    public function values() {
        return $this->hasMany(Value::class, 'student_id');
    }
}
