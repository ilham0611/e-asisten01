<?php

namespace App\Model\App\Master;

use App\Model\App\Master\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assistant extends Model
{
    use SoftDeletes;
    public $incrementing  = false;
    protected $dates = ['deleted_at'];
    protected $table = 'master.students';
    protected $connection = 'Master';
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function students() {
        return $this->hasMany(Student::class, 'assistant_id');
    }
}
