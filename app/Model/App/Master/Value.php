<?php

namespace App\Model\App\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Value extends Model
{
    use SoftDeletes;
    public $incrementing  = false;
    protected $dates = ['deleted_at'];
    protected $table = 'master.values';
    protected $connection = 'Master';
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
