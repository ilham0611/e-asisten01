<?php

namespace App\Http\Controllers\App\Master;

use App\User as Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssistantController extends Controller
{
    public function index() {
        $users = Model::orderBy('name', 'ASC')->get();
        return view('app.asisten.index', compact('users'));
    }
}
