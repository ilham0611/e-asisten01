<?php

namespace App\Http\Controllers\App\Master;

use App\Model\App\Master\Value as Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class ValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Model();
        $model->student_id = $request->get('student_id');
        $model->meeting = $request->get('meeting');
        $model->responsValue = $request->get('responsValue');
        $model->preliminaryAssignmentValue = $request->get('preliminaryAssignmentValue');
        $model->practiceValue = $request->get('practiceValue');
        $model->created_by = Auth::user()->id;
        $model->update_by = Auth::user()->id;
        $model->save();
        return redirect()->route('studentDetail',$model->student_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Model $value, Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model $value)
    {
        $value->meeting = $request->get('meeting');
        $value->responsValue = $request->get('responsValue');
        $value->preliminaryAssignmentValue = $request->get('preliminaryAssignmentValue');
        $value->practiceValue = $request->get('practiceValue');
        $value->created_by = Auth::user()->id;
        $value->update_by = Auth::user()->id;
        $value->save();
        return redirect()->route('studentDetail',$value->student_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
