<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::connection('Master')->hasTable('values')) {
            Schema::connection('Master')->create('values', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('student_id');
                $table->integer('meeting');
                $table->string('responsValue');
                $table->string('preliminaryAssignmentValue');
                $table->string('practiceValue');
                $table->uuid('created_by');
                $table->uuid('update_by');
                $table->timestamps();
                $table->softDeletes();
            });
            DB::statement('ALTER TABLE ONLY master.values ALTER COLUMN id SET DEFAULT uuid_generate_v4()');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('Master')->drop('values');
    }
}
