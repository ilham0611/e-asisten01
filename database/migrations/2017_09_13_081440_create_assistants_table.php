<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         if(!Schema::connection('Master')->hasTable('assistants')) {
             Schema::connection('Master')->create('assistants', function (Blueprint $table) {
                 $table->uuid('id')->primary();
                 $table->uuid('user_id');
                 $table->uuid('created_by');
                 $table->uuid('update_by');
                 $table->timestamps();
                 $table->softDeletes();
             });
             DB::statement('ALTER TABLE ONLY master.assistants ALTER COLUMN id SET DEFAULT uuid_generate_v4()');
         }
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::connection('Master')->drop('assistants');
     }
}
