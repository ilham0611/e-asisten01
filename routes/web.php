<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'App', 'prefix' => 'app'], function () {
    Route::group(['namespace' => 'Master', 'prefix' => 'master'], function () {
        Route::resource('myStudent', 'MyStudentController', [
            'names' => [
                'index'   => 'app.master.myStudent.index',
                'create'  => 'app.master.myStudent.create',
                'store'   => 'app.master.myStudent.store',
                'edit'    => 'app.master.myStudent.edit',
                'update'  => 'app.master.myStudent.update',
                'destroy' => 'app.master.myStudent.destroy'
            ],
            'except'=>['show']
        ]);
        Route::get('studentDetail/{student}', 'MyStudentController@detail')->name('studentDetail');
    });
});
Route::group(['namespace' => 'App', 'prefix' => 'app'], function () {
    Route::group(['namespace' => 'Master', 'prefix' => 'master'], function () {
        Route::resource('assistant', 'AssistantController', [
            'names' => [
                'index'   => 'app.master.assistant.index',
                'create'  => 'app.master.assistant.create',
                'store'   => 'app.master.assistant.store',
                'edit'    => 'app.master.assistant.edit',
                'update'  => 'app.master.assistant.update',
                'destroy' => 'app.master.assistant.destroy'
            ],
            'except'=>['show']
        ]);
    });
});
Route::group(['namespace' => 'App', 'prefix' => 'app'], function () {
    Route::group(['namespace' => 'Master', 'prefix' => 'master'], function () {
        Route::get('/recap', 'RecapController@index')->name('app.recap');
    });
});
Route::group(['namespace' => 'App', 'prefix' => 'app'], function () {
    Route::group(['namespace' => 'Master', 'prefix' => 'master'], function () {
        Route::resource('value', 'ValueController', [
            'names' => [
                'index'   => 'app.master.value.index',
                'create'  => 'app.master.value.create',
                'store'   => 'app.master.value.store',
                'edit'    => 'app.master.value.edit',
                'update'  => 'app.master.value.update',
                'destroy' => 'app.master.value.destroy'
            ],
            'except'=>['show']
        ]);
    });
});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/asisten', function () {
    return view('app.asisten.index');
})->name('asisten');

Route::get('/praktikan-saya', function () {
    return view('app.asisten.my_praktikan');
})->name('my_praktikan');
Route::get('/prodi', function () {
    return view('app.reference.prodi');
})->name('prodi');
Route::get('/course', function () {
    return view('app.asisten.course');
})->name('course');
